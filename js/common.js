﻿

/* ================================================================ */
/* Top Openning */

var period = 1; // 有効期限日数
 
$(function() {
    if($.cookie('access') == undefined) {
        $.cookie('access', 'on', { expires: period });

		jQuery.event.add(window,"load",function() { // 全ての読み込み完了後に呼ばれる関数
			setTimeout(function() {
				$('#wrap').fadeIn(500);
			}, 10);
		});

    } else {

		$("#wrap").css("display","block");

    }
    // cookieの削除
    $(document).on('click', '#del', function() {
        $.removeCookie('access');
    });
});



/* ================================================================ */
/* Page Scroller */

// jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
// Open source under the BSD License. 
// Copyright (c) 2008 George McGinley Smith
// All rights reserved.
jQuery.easing.quart = function(x,t,b,c,d){
	return (t==d) ? b+c : c*(-Math.pow(2,-10*t/d)+1)+b;
}
$(document).ready(function(){
	$('a[href^=#]').click(function(){
		if(location.pathname.replace(/^\//,'')==this.pathname.replace(/^\//,'') && location.hostname==this.hostname){
			var $target = $(this.hash);
			var targetObj = navigator.appName.match(/Opera/) ? 'html':'html,body';
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if($target.length){
				if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
					var targetOffset = $target.offset().top - 53;
				}else{
					var targetOffset = $target.offset().top - 150;
				}
				$(targetObj).animate({scrollTop:targetOffset},1200,'quart');
				return false;
			}
		}
	});
});
$(document).ready(function(){
	$('area[href^=#]').click(function(){
		if(location.pathname.replace(/^\//,'')==this.pathname.replace(/^\//,'') && location.hostname==this.hostname){
			var $target = $(this.hash);
			var targetObj = navigator.appName.match(/Opera/) ? 'html':'html,body';
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if($target.length){
				if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
					var targetOffset = $target.offset().top - 53;
				}else{
					var targetOffset = $target.offset().top - 150;
				}
				$(targetObj).animate({scrollTop:targetOffset},1200,'quart');
				return false;
			}
		}
	});
});



/* ================================================================ */
/* Rollover : Crossfade & Swapimage */

// Crossfade
// rollover2.js  Copyright(c) 2007 KAZUMiX
// http://d.hatena.ne.jp/KAZUMiX/20071017/rollover2
// Licensed under the MIT License:
// http://www.opensource.org/licenses/mit-license.php
(function(){
	var rolloverImages = [];
	function setRollOver2(){
		if(!document.images){return;}
		var imgs = document.images;
		var insert = [];
		for(var i=0,len=imgs.length; i<len; i++){
			var splitname = imgs[i].src.split('_off.');
			if(splitname[1]){
				var rolloverImg = document.createElement('img');
				rolloverImages.push(rolloverImg);
				rolloverImg.src = splitname[0]+'_on.'+splitname[1];
				var alpha = 0;
				rolloverImg.currentAlpha = alpha;
				rolloverImg.style.opacity = alpha/100;
				rolloverImg.style.filter = 'alpha(opacity='+alpha+')';
				rolloverImg.style.position = 'absolute';
				addEvent(rolloverImg,'mouseover',function(){setFader(this,100);});
				addEvent(rolloverImg,'mouseout',function(){setFader(this,0);});
				insert[insert.length] = {position:imgs[i],element:rolloverImg};
			}
		}
		for(i=0,len=insert.length; i<len ;i++){
			var parent = insert[i].position.parentNode;
			parent.insertBefore(insert[i].element,insert[i].position);
		}
		addEvent(window,'beforeunload', clearRollover);
	}
	function setFader(targetObj,targetAlpha){
		targetObj.targetAlpha = targetAlpha;
		if(targetObj.currentAlpha==undefined){
			targetObj.currentAlpha = 100;
		}
		if(targetObj.currentAlpha==targetObj.targetAlpha){
			return;
		}
		if(!targetObj.fading){
			if(!targetObj.fader){
				targetObj.fader = fader;
			}
			targetObj.fading = true;
			targetObj.fader();
		}
	}
	function fader(){
		this.currentAlpha += (this.targetAlpha - this.currentAlpha)*0.2;
		if(Math.abs(this.currentAlpha-this.targetAlpha)<1){
			this.currentAlpha = this.targetAlpha;
			this.fading = false;
		}
		var alpha = parseInt(this.currentAlpha);
		this.style.opacity = alpha/100;
		this.style.filter = 'alpha(opacity='+alpha+')';
		if(this.fading){
			var scope = this;
			setTimeout(function(){fader.apply(scope)},10);
		}
	}
	function clearRollover(){
		for(var i=0,len=rolloverImages.length; i<len; i++){
			var image = rolloverImages[i];
			image.style.opacity = 0;
			image.style.filter = 'alpha(opacity=0)';
		}
	}
	function addEvent(eventTarget, eventName, func){
		if(eventTarget.addEventListener){
			eventTarget.addEventListener(eventName, func, false);
		}else if(window.attachEvent){
			eventTarget.attachEvent('on'+eventName, function(){func.apply(eventTarget);});
		}
	}
	addEvent(window,'load',setRollOver2);
})();

// Swapimage
(function(onLoad){
	try{
		window.addEventListener('load', onLoad, false);
	}catch(e){
		window.attachEvent('onload', onLoad);
	}
})(function(){
	var tags=['img','input'];
	for(var i=0; i<tags.length; i++){
		var over = function(){
			this.src=this.src.replace('_off_.','_on_.'); 
		}
		var out = function(){ 
			this.src=this.src.replace('_on_.','_off_.');
		}
		var el=document.getElementsByTagName(tags[i]);
		for(var ii=0; ii<el.length; ii++){
			if(!el[ii].src.match(/_off_\./)) continue;
			el[ii].onmouseover = over;
			el[ii].onmouseout = out;
		}
	}
});



/* ================================================================ */
/* Popup Window */

$(document).ready(function(){
	$('a[class^=popupJs],area[class^=popupJs]').click(function(){
		var iUrl = $(this).attr('href');
		var iArr = $(this).attr('rel').split(',');
		if(iArr[2]==undefined || iArr[2]=='') iArr[2] = 'popupWin';
		var iWinObj = window.open(iUrl,iArr[2],'width='+ iArr[0] +',height='+ iArr[1] +',toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
		iWinObj.blur();
		iWinObj.focus();
		return false;
	});
});

function goMainWin(url){
	if(!window.opener || window.opener.closed){
		window.alert('メインウィンドウがありません');
	}
	else{
		window.opener.location.href = url;
		window.close();
	}
}



/* ================================================================ */
/* Pagetop Button */

$(function() {
	var pageTop = $('#pagetopBtn');	
	$(window).scroll(function () {
		if ($(this).scrollTop() > 150) {
			pageTop.fadeIn();
		} else {
			pageTop.fadeOut();
		}
	});
});



/* ================================================================ */
/* 透過PNG用 */

// for IE
jQuery(function() {
    if(navigator.userAgent.indexOf("MSIE") != -1) {
        jQuery('img').each(function() {
            if(jQuery(this).attr('src').indexOf('.png') != -1) {
                jQuery(this).css({
                    'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + jQuery(this).attr('src') + '", sizingMethod="scale");'
                });
            }
        });
    }
});



/* ================================================================ */
/* プリント用 */

function PrintPage(){
	if(document.getElementById || document.layers){
		window.print();	
	}
}



/* ================================================================ */
/* アニメーション（1） */

$(function(){
	$('#sec01 .btnBox .img').css("opacity","0");
	$(window).scroll(function (){
		$("#sec01 .btnBox").each(function(){
			var imgPos = $(this).offset().top;    
			var scroll = $(window).scrollTop();
			var windowHeight = $(window).height();
			if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
				if (scroll > imgPos - windowHeight + windowHeight/5){
					$(".img",this).css({"opacity":"1","top":"0px"});
				} else {
					$(".img",this).css({"opacity":"0","top":"38px"});
				}
			} else {
				if (scroll > imgPos - windowHeight + windowHeight/3){
					$(".img",this).css({"opacity":"1","top":"0px"});
				} else {
					$(".img",this).css({"opacity":"0","top":"80px"});
				}
			}
		});
	});
});



/* ================================================================ */
/* tab */

$(document).ready(function(){
	$("#seBox").css("display","none");
	$("#neBtn").click(function () {
		$("#neBox").css("display","block");
		$("#seBox").css("display","none");
		$("#seBtn").removeClass('select');
		$(this).addClass('select');
	});
	$("#seBtn").click(function () {
		$("#seBox").css("display","block");
		$("#neBox").css("display","none");
		$("#neBtn").removeClass('select');
		$(this).addClass('select');
	});
});



/* ================================================================ */
/* アニメーション（2） */

$(function(){
	$('#sec03 #animeBox .human').css("opacity","0");
	$(window).scroll(function (){
		$("#sec03 #animeBox").each(function(){
			var imgPos = $(this).offset().top;    
			var scroll = $(window).scrollTop();
			var windowHeight = $(window).height();
			if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
				if (scroll > imgPos - windowHeight + windowHeight/4){
					$(".human",this).css("opacity","1" );
					$(".human",this).css("animation","jump 2.5s 0s 1" );
				}
			} else {
				if (scroll > imgPos - windowHeight + 579){
					$(".human",this).css("opacity","1" );
					$(".human",this).css("animation","jump 2.5s 0s 1" );
				}
			}
		});
	});
});



/* ================================================================ */
/* アニメーション（3） */

$(function(){
  $('#sec05_2 .box').css("opacity","0");
  $(window).scroll(function (){
    $("#sec05_2 .box").each(function(){
      var imgPos = $(this).offset().top;    
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if (scroll > imgPos - windowHeight + windowHeight/2){
        $(this).css("opacity","1" );
      } else {
        $(this).css("opacity","0" );
      }
    });
  });
});



/* ================================================================ */
/* SP用 */

if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {


	document.write('<link rel="stylesheet" media="all" href="/css/sp.css">');
	// document.write('<link rel="stylesheet" media="all" href="/careetec/css/sp.css">');


	jQuery.event.add(window,"load",function() { // 全ての読み込み完了後に呼ばれる関数
	
		function cssRewrit(){
			var winW = document.documentElement.clientWidth;
			var winH = window.innerHeight ? window.innerHeight: $(window).height();
			$('#sec01 .mainImg').css({'height':winW/640*690});
		}
		$(window).resize(function(){
			cssRewrit()
		});
		cssRewrit();
	
	});
	
	/* Nav for SP */
	
	$(document).ready(function(){
		$("#navBtn, header nav li a").click(function () {
			$("header nav ul").slideToggle('slow');
		});
	});
	
	/* lightBox Delete */
	
	$("#sec05 li p.img img").unwrap();
	
	/* Tel Link */
	
	$('.telBox p.tel').wrapInner(function(index) {
		return '<a href="tel:' + $(this).text() + '">';
	});

} 


// newspage
$(function() {

	var hash = location.hash;
	//hashに要素が存在する場合、hashで取得した文字列（#tab1,#tab2等）から#より後を取得(tab1,tab2)
	if($(hash).length){
		var tabname = hash.slice(1) ;
	} else{ // 要素が存在しなければtabnameにbox1を代入する
		var tabname = "neBtn";
	}
	switch (tabname) {
		case 'neBtn':
			// 何もしない
			break;
		case 'seBtn':
			$('.tab .tab-nav').removeClass('select');
			$('.tab .tab-nav:nth-of-type(2)').addClass('select');
			$('.tab-contents .contentBox').removeClass('select').hide().stop();
			$('.tab-contents .contentBox:nth-of-type(2)').css('display','block');
				var hash2 = location.hash;
				var header = $("header").height();
				if (location.hash) {
					setTimeout(function(){// 0.1秒後に実行
						var target = $(hash2 == "#" || hash2 == "" ? 'html' : hash2);
						// 移動先を数値で取得
						var position = target.offset().top;
					  	$('body,html').animate({scrollTop:position-header}, 0);
					}, 100);
				} else {
					return false;
				}
			break;
	}
});

//youtube
var tag = document.createElement('script');//タグ生成
tag.src = "https://www.youtube.com/iframe_api";//タグのsrc指定
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// YouTubeの埋め込み
function onYouTubeIframeAPIReady() {
  ytPlayer = new YT.Player(
    'player', // 埋め込む場所の指定
    {
      videoId: 'XwHF1xOD4b4', // YouTubeのID
      playerVars: {
        // loop: 1,//0:ループしない 1:ループする 1の場合playlist設定必須
        playlist: 'Az7R2yzmG_U',//次に流すYoutubeのID
        controls: 1,//コントローラー
        // autoplay: 1,//オートプレイ
        showinfo: 0//動画タイトルなど
      },
      events: {
        'onReady': onPlayerReady
      }
    }
  );
}
//プレイ準備完了後
function onPlayerReady(event) {
  // event.target.playVideo();
  event.target.mute();
}