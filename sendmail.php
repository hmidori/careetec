<?php header("Content-Type:text/html;charset=utf-8"); ?>
<?php //error_reporting(E_ALL | E_STRICT);
##-----------------------------------------------------------------------------------------------------------------##
#
#  PHPメールプログラム　フリー版 最終更新日2014/12/12
#　改造や改変は自己責任で行ってください。
#	
#  今のところ特に問題点はありませんが、不具合等がありましたら下記までご連絡ください。
#  MailAddress: info@php-factory.net
#  name: K.Numata
#  HP: http://www.php-factory.net/
#
#  重要！！サイトでチェックボックスを使用する場合のみですが。。。
#  チェックボックスを使用する場合はinputタグに記述するname属性の値を必ず配列の形にしてください。
#  例　name="当サイトをしったきっかけ[]"  として下さい。
#  nameの値の最後に[と]を付ける。じゃないと複数の値を取得できません！
#
##-----------------------------------------------------------------------------------------------------------------##
if (version_compare(PHP_VERSION, '5.1.0', '>=')) {//PHP5.1.0以上の場合のみタイムゾーンを定義
	date_default_timezone_set('Asia/Tokyo');//タイムゾーンの設定（日本以外の場合には適宜設定ください）
}
/*-------------------------------------------------------------------------------------------------------------------
* ★以下設定時の注意点　
* ・値（=の後）は数字以外の文字列（一部を除く）はダブルクオーテーション「"」、または「'」で囲んでいます。
* ・これをを外したり削除したりしないでください。後ろのセミコロン「;」も削除しないください。
* ・また先頭に「$」が付いた文字列は変更しないでください。数字の1または0で設定しているものは必ず半角数字で設定下さい。
* ・メールアドレスのname属性の値が「Email」ではない場合、以下必須設定箇所の「$Email」の値も変更下さい。
* ・name属性の値に半角スペースは使用できません。
*以上のことを間違えてしまうとプログラムが動作しなくなりますので注意下さい。
-------------------------------------------------------------------------------------------------------------------*/


//---------------------------　必須設定　必ず設定してください　-----------------------

//サイトのトップページのURL　※デフォルトでは送信完了後に「トップページへ戻る」ボタンが表示されますので
$site_top = "index.html";

// 管理者メールアドレス ※メールを受け取るメールアドレス(複数指定する場合は「,」で区切ってください 例 $to = "aa@aa.aa,bb@bb.bb";)
$to = "jinji_official@kcct.co.jp";
// $to = "m-hashimoto@azure-plus.co.jp,kizu@azure-plus.co.jp";

//フォームのメールアドレス入力箇所のname属性の値（name="○○"　の○○部分）
$Email = "メールアドレス";

/*------------------------------------------------------------------------------------------------
以下スパム防止のための設定　
※有効にするにはこのファイルとフォームページが同一ドメイン内にある必要があります
------------------------------------------------------------------------------------------------*/

//スパム防止のためのリファラチェック（フォームページが同一ドメインであるかどうかのチェック）(する=1, しない=0)
$Referer_check = 0;

//リファラチェックを「する」場合のドメイン ※以下例を参考に設置するサイトのドメインを指定して下さい。
$Referer_check_domain = "php-factory.net";

//---------------------------　必須設定　ここまで　------------------------------------


//---------------------- 任意設定　以下は必要に応じて設定してください ------------------------


// 管理者宛のメールで差出人を送信者のメールアドレスにする(する=1, しない=0)
// する場合は、メール入力欄のname属性の値を「$Email」で指定した値にしてください。
//メーラーなどで返信する場合に便利なので「する」がおすすめです。
$userMail = 1;

// Bccで送るメールアドレス(複数指定する場合は「,」で区切ってください 例 $BccMail = "aa@aa.aa,bb@bb.bb";)
$BccMail = "";

// 管理者宛に送信されるメールのタイトル（件名）
$subject = "【KCCT】エントリー";

// 送信確認画面の表示(する=1, しない=0)
$confirmDsp = 1;

// 送信完了後に自動的に指定のページ(サンクスページなど)に移動する(する=1, しない=0)
// CV率を解析したい場合などはサンクスページを別途用意し、URLをこの下の項目で指定してください。
// 0にすると、デフォルトの送信完了画面が表示されます。
$jumpPage = 0;

// 送信完了後に表示するページURL（上記で1を設定した場合のみ）※httpから始まるURLで指定ください。
$thanksPage = "http://xxx.xxxxxxxxx/thanks.html";

// 必須入力項目を設定する(する=1, しない=0)
$requireCheck = 1;

/* 必須入力項目(入力フォームで指定したname属性の値を指定してください。（上記で1を設定した場合のみ）
値はシングルクォーテーションで囲み、複数の場合はカンマで区切ってください。フォーム側と順番を合わせると良いです。 
配列の形「name="○○[]"」の場合には必ず後ろの[]を取ったものを指定して下さい。*/
$require = array('氏名','メールアドレス', '性別', '生年月日_年', '生年月日_月', '生年月日_日', '住所', '電話番号', '希望職種', '上記の「個人情報の取り扱いについて」に同意する');


//----------------------------------------------------------------------
//  自動返信メール設定(START)
//----------------------------------------------------------------------

// 差出人に送信内容確認メール（自動返信メール）を送る(送る=1, 送らない=0)
// 送る場合は、フォーム側のメール入力欄のname属性の値が上記「$Email」で指定した値と同じである必要があります
$remail = 1;

//自動返信メールの送信者欄に表示される名前　※あなたの名前や会社名など（もし自動返信メールの送信者名が文字化けする場合ここは空にしてください）
$refrom_name = "";

// 差出人に送信確認メールを送る場合のメールのタイトル（上記で1を設定した場合のみ）
$re_subject = "【KCCT】エントリーを受け付けました";

//フォーム側の「名前」箇所のname属性の値　※自動返信メールの「○○様」の表示で使用します。
//指定しない、または存在しない場合は、○○様と表示されないだけです。あえて無効にしてもOK
$dsp_name = '氏名';

//自動返信メールの冒頭の文言 ※日本語部分のみ変更可
$remail_text = <<< TEXT

ご応募ありがとうございました。
担当よりご連絡させていただきますので、今しばらくお待ちください。

送信内容は以下になります。

TEXT;


//自動返信メールに署名（フッター）を表示(する=1, しない=0)※管理者宛にも表示されます。
$mailFooterDsp = 0;

//上記で「1」を選択時に表示する署名（フッター）（FOOTER～FOOTER;の間に記述してください）
$mailSignature = <<< FOOTER

──────────────────────
株式会社○○○○　佐藤太郎
〒150-XXXX 東京都○○区○○ 　○○ビル○F　
TEL：03- XXXX - XXXX 　FAX：03- XXXX - XXXX
携帯：090- XXXX - XXXX 　
E-mail:xxxx@xxxx.com
URL: http://www.php-factory.net/
──────────────────────

FOOTER;


//----------------------------------------------------------------------
//  自動返信メール設定(END)
//----------------------------------------------------------------------

//メールアドレスの形式チェックを行うかどうか。(する=1, しない=0)
//※デフォルトは「する」。特に理由がなければ変更しないで下さい。メール入力欄のname属性の値が上記「$Email」で指定した値である必要があります。
$mail_check = 1;

//全角英数字→半角変換を行うかどうか。(する=1, しない=0)
$hankaku = 0;

//全角英数字→半角変換を行う項目のname属性の値（name="○○"の「○○」部分）
//※複数の場合にはカンマで区切って下さい。（上記で「1」を指定した場合のみ有効）
//配列の形「name="○○[]"」の場合には必ず後ろの[]を取ったものを指定して下さい。
$hankaku_array = array('電話番号','金額');


//------------------------------- 任意設定ここまで ---------------------------------------------


// 以下の変更は知識のある方のみ自己責任でお願いします。


//----------------------------------------------------------------------
//  関数実行、変数初期化
//----------------------------------------------------------------------
$encode = "UTF-8";//このファイルの文字コード定義（変更不可）

if(isset($_GET)) $_GET = sanitize($_GET);//NULLバイト除去//
if(isset($_POST)) $_POST = sanitize($_POST);//NULLバイト除去//
if(isset($_COOKIE)) $_COOKIE = sanitize($_COOKIE);//NULLバイト除去//
if($encode == 'SJIS') $_POST = sjisReplace($_POST,$encode);//Shift-JISの場合に誤変換文字の置換実行
$funcRefererCheck = refererCheck($Referer_check,$Referer_check_domain);//リファラチェック実行

//変数初期化
$sendmail = 0;
$empty_flag = 0;
$post_mail = '';
$errm ='';
$header ='';

if($requireCheck == 1) {
	$requireResArray = requireCheck($require);//必須チェック実行し返り値を受け取る
	$errm = $requireResArray['errm'];
	$empty_flag = $requireResArray['empty_flag'];
}
//メールアドレスチェック
if(empty($errm)){
	foreach($_POST as $key=>$val) {
		if($val == "confirm_submit") $sendmail = 1;
		if($key == $Email) $post_mail = h($val);
		if($key == $Email && $mail_check == 1 && !empty($val)){
			if(!checkMail($val)){
				$errm .= "<p class=\"error_messe\">【".$key."】はメールアドレスの形式が正しくありません。</p>\n";
				$empty_flag = 1;
			}
		}
	}
}
  
if(($confirmDsp == 0 || $sendmail == 1) && $empty_flag != 1){
	
	//差出人に届くメールをセット
	if($remail == 1) {
		$userBody = mailToUser($_POST,$dsp_name,$remail_text,$mailFooterDsp,$mailSignature,$encode);
		$reheader = userHeader($refrom_name,$to,$encode);
		$re_subject = "=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($re_subject,"JIS",$encode))."?=";
	}
	//管理者宛に届くメールをセット
	$adminBody = mailToAdmin($_POST,$subject,$mailFooterDsp,$mailSignature,$encode,$confirmDsp);
	$header = adminHeader($userMail,$post_mail,$BccMail,$to);
	$subject = "=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($subject,"JIS",$encode))."?=";
	
	mail($to,$subject,$adminBody,$header);
	if($remail == 1) mail($post_mail,$re_subject,$userBody,$reheader);
}
else if($confirmDsp == 1){ 

/*　▼▼▼送信確認画面のレイアウト※編集可　オリジナルのデザインも適用可能▼▼▼　*/
?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keyword" content="KCCT,キャリア,コンサル,転職,エンジニア,研修,育成,スキル">
<meta name="Description" content="KCCSキャリアテック（KCCT）は、通信インフラや企業ネットワークを得意分野とするエンジニアリングサービス会社で京セラグループの一員です。社員一人ひとりの可能性を尊重し、新しい世界にチャレンジしたい方、自分の可能性を見つけたい方の採用に注力しています。">
<meta property="og:url" content="https://www.kcct.co.jp/careetec/">
<meta property="og:type" content="website">
<meta property="og:image" content="https://www.kcct.co.jp/careetec/img/share.png">
<meta property="og:title" content="未経験からのITキャリアを支援する「キャリテク！」">
<meta property="og:description" content="ITエンジニアってこんな業界！自分らしくエンジニアになるには？">
<meta property="og:locale" content="ja_JP">
<title>KCCT｜京セラグループのKCCSキャリアテック！</title>
<link rel="stylesheet" media="all" href="./css/layout.css">
<link rel="stylesheet" media="print" href="./css/print.css">
<!-- favicon -->
<link rel="icon" type="image/x-icon" href="/careetec/favicon.ico">
<!--[if lte IE 9]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-785969-11', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1098546120231418');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1098546120231418&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- YTM Code -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=qjHVA9g&referrer=" + encodeURIComponent(document.location.href) + "";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=qjHVA9g" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!-- End YTM Code -->
</head>

<body>
<div id="wrap">

<header>
<div class="inner">
<h1><a href="./"><img src="./img/logo.gif" alt="KCCT"></a></h1>
<p class="icn01">
	<a href="https://privacymark.jp/" target="_blank"><img src="./img/logo-head.png" alt=""></a>
</p>
<p class="jets">
	<a href="https://chibajets.jp/" target="_blank"><img src="./img/logo-jets.png" alt="" class="pc"></a>
	<a href="https://chibajets.jp/" target="_blank"><img src="./img/logo-jets_sp.png" alt="" class="sp"></a>
</p>
<!-- <p class="icn01"><img src="./img/logo-foot.png" alt=""></p> -->
<!-- <p class="icn02"><img src="./img/img02.gif" alt=""></p> -->
<p class="entryBtn"><a href="./#sec06"><img src="./img/btn01.gif" alt="【3分で完了】エントリーはこちら"></a></p>
<nav>
<ul>
<li><a href="./#sec01">ITエンジニアを目指す</a></li>
<li class="short"><a href="./#sec02">ITエンジニア研修</a></li>
<li><a href="./#sec03">卒業後のイメージ</a></li>
<li class="short"><a href="./#sec04">活躍しているセンパイ</a></li>
<li><a href="./#sec05">KCCTを知る</a></li>
<li class="seminar"><a href="seminar.html">セミナー</a></li>
<li><a href="./news/2020/">ニュース</a></li>
</ul>
</nav>
<!--/inner--></div>
</header>

<section id="content" class="confirm">

<section id="sec06">
<div class="inner">
<h2><img src="./img/tit05.gif" alt="エントリーフォーム"></h2>
                
<?php if($empty_flag == 1){ ?>

<p class="lead">入力にエラーがあります。<br>
下記をご確認の上「戻る」ボタンにて修正をお願い致します。</p>
<div class="box">

<div class="errTxt">
<?php echo $errm; ?>
<!--/errTxt--></div>
<p class="link"><a href="javascript:history.back();">前画面に戻る</a></p>

<!--/box--></div>

<?php }else{ ?>

<p class="lead">以下の内容で間違いがなければ、「送信する」ボタンを押してください。</p>
<form action="<?php echo h($_SERVER['SCRIPT_NAME']); ?>" method="POST">
<div class="box">

<table class="formTable">
<?php echo confirmOutput($_POST);//入力内容を表示?>
</table>

<p class="link2"><a href="javascript:history.back();">前画面に戻る</a></p>

<!--/box--></div>

<div class="btnCheck">
<input type="hidden" name="mail_set" value="confirm_submit">
<input type="hidden" name="httpReferer" value="<?php echo h($_SERVER['HTTP_REFERER']);?>">
<button type="submit">送信する</button>
<!--/btnCheck--></div>
</form>

<?php } ?>

<!--/inner--></div>
<!--/sec06--></section>

<!--/content--></section>

<p id="pagetopBtn"><a href="#wrap"><img src="./img/btn_pagetop.png" alt="ページの先頭へ戻る"></a></p>

<footer>
<div class="inner">
<p class="logo"><img src="./img/logo.gif" alt="KCCT"></p>
<div class="telBox tokyo">
<p class="add">東京本社</p>
<p class="tel">0800-123-0451</p>
<!--/tokyo--></div>
<div class="telBox osaka">
<p class="add">大阪支社</p>
<p class="tel">0800-500-0451</p>
<!--/osaka--></div>
<p class="mailBox"><a href="mailto:jinji_official@kcct.co.jp">メールでのお問い合わせはこちら</a></p>
<!-- <p class="privacy"><a href="https://privacymark.jp/" target="_blank"><img src="./img/logo-foot.png" alt=""></a></p> -->
<div class="mark">
	<p class="privacy"><a href="https://privacymark.jp/" target="_blank"><img src="./img/logo-foot.png" alt=""></a></p>
	<p class="jets"><a href="https://chibajets.jp/" target="_blank"><img src="./img/logo-jets.png" alt=""></a></p>
</div>
<!--/inner--></div>
<div class="footNav">
<ul>
<li><a href="/company/index.html" target="_blank">会社情報</a></li>
<li><a href="/privacy.html" target="_blank">個人情報保護方針</a></li>
<li><a href="https://www.kcct.co.jp/handle.html" target="_blank">個人情報の取扱について</a></li>
</ul>
<p class="copy">Copyright © 2016 KCCS Career tech Inc. All Rights Reserved.</p>
<!--/footNav--></div>
</footer>

<!--/wrap--></div>

<script src="./js/jquery-1.7.1.min.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/common.js"></script>

</body>
</html>
<?php
/* ▲▲▲送信確認画面のレイアウト　※オリジナルのデザインも適用可能▲▲▲　*/
}

if(($jumpPage == 0 && $sendmail == 1) || ($jumpPage == 0 && ($confirmDsp == 0 && $sendmail == 0))) { 

/* ▼▼▼送信完了画面のレイアウト　編集可 ※送信完了後に指定のページに移動しない場合のみ表示▼▼▼　*/
?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keyword" content="KCCT,キャリア,コンサル,転職,エンジニア,研修,育成,スキル">
<meta name="Description" content="KCCSキャリアテック（KCCT）は、通信インフラや企業ネットワークを得意分野とするエンジニアリングサービス会社で京セラグループの一員です。社員一人ひとりの可能性を尊重し、新しい世界にチャレンジしたい方、自分の可能性を見つけたい方の採用に注力しています。">
<meta property="og:url" content="https://www.kcct.co.jp/careetec/">
<meta property="og:type" content="website">
<meta property="og:image" content="https://www.kcct.co.jp/careetec/img/share.png">
<meta property="og:title" content="未経験からのITキャリアを支援する「キャリテク！」">
<meta property="og:description" content="ITエンジニアってこんな業界！自分らしくエンジニアになるには？">
<meta property="og:locale" content="ja_JP">
<title>KCCT｜京セラグループのKCCSキャリアテック！</title>
<link rel="stylesheet" media="all" href="./css/layout.css">
<link rel="stylesheet" media="print" href="./css/print.css">
<!-- favicon -->
<link rel="icon" type="image/x-icon" href="/careetec/favicon.ico">
<!--[if lte IE 9]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-785969-11', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1098546120231418');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1098546120231418&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- YTM Code -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=qjHVA9g&referrer=" + encodeURIComponent(document.location.href) + "";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=qjHVA9g" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!-- End YTM Code -->
</head>

<body>
<div id="wrap">

<header>
<div class="inner">
<h1><a href="./"><img src="./img/logo.gif" alt="KCCT"></a></h1>
<p class="icn01"><img src="./img/logo-foot.png" alt=""></p>
<!-- <p class="icn02"><img src="./img/img02.gif" alt=""></p> -->
<p class="entryBtn"><a href="./#sec06"><img src="./img/btn01.gif" alt="【3分で完了】エントリーはこちら"></a></p>
<nav>
<ul>
<li><a href="./#sec01">ITエンジニアを目指す</a></li>
<li class="short"><a href="./#sec02">ITエンジニア研修</a></li>
<li><a href="./#sec03">卒業後のイメージ</a></li>
<li class="short"><a href="./#sec04">活躍しているセンパイ</a></li>
<li><a href="./#sec05">KCCTを知る</a></li>
<li class="seminar"><a href="seminar.html">セミナー</a></li>
<li><a href="./news/2020/">ニュース</a></li>
</ul>
</nav>
<!--/inner--></div>
</header>

<section id="content" class="thanks">

<section id="sec06">
<div class="inner">
<h2><img src="./img/tit05.gif" alt="エントリーフォーム"></h2>

<div class="box">


<?php if($empty_flag == 1){ ?>

<p class="txt2">入力にエラーがあります。<br>
下記をご確認の上「戻る」ボタンにて修正をお願い致します。</p>
<div class="errTxt">
<?php echo $errm; ?>
<!--/errTxt--></div>
<p class="link"><a href="javascript:history.back();">前画面に戻る</a></p>

<?php }else{ ?>

<p class="txt">エントリーありがとうございます。<br>
送信は正常に完了いたしました。</p>
<!-- <p class="txt" style="color: red;">【年末年始休業のお知らせ】<br>
誠に勝手ながら、12月28日（土）から2020年1月5日（日）までの間、休業とさせていただきます。<br>
なお、この期間中の採用エントリー及びWebサイト等のお問い合わせは、<br>
1月6日（月）以降のご回答とさせていただきます。<br>
ご迷惑をおかけいたしますが、何卒ご了承いただきますようお願い申し上げます。
</p> -->
<p class="link"><a href="<?php echo $site_top ;?>">トップページへ戻る</a></p>

<?php } ?>


<!--/box--></div>

<!--/inner--></div>
<!--/sec06--></section>

<!--/content--></section>

<p id="pagetopBtn"><a href="#wrap"><img src="./img/btn_pagetop.png" alt="ページの先頭へ戻る"></a></p>

<footer>
<div class="inner">
<p class="logo"><img src="./img/logo.gif" alt="KCCT"></p>
<div class="telBox tokyo">
<p class="add">東京本社</p>
<p class="tel">0800-123-0451</p>
<!--/tokyo--></div>
<div class="telBox osaka">
<p class="add">大阪支社</p>
<p class="tel">0800-500-0451</p>
<!--/osaka--></div>
<p class="mailBox"><a href="mailto:jinji_official@kcct.co.jp">メールでのお問い合わせはこちら</a></p>
<!-- <p class="privacy"><a href="https://privacymark.jp/" target="_blank"><img src="./img/logo-foot.png" alt=""></a></p> -->
<div class="mark">
	<p class="privacy"><a href="https://privacymark.jp/" target="_blank"><img src="./img/logo-foot.png" alt=""></a></p>
	<p class="jets"><a href="https://chibajets.jp/" target="_blank"><img src="./img/logo-jets.png" alt=""></a></p>
</div>
<!--/inner--></div>
<div class="footNav">
<ul>
<li><a href="/company/index.html" target="_blank">会社情報</a></li>
<li><a href="/privacy.html" target="_blank">個人情報保護方針</a></li>
<li><a href="https://www.kcct.co.jp/handle.html" target="_blank">個人情報の取扱について</a></li>
</ul>
<p class="copy">Copyright © 2016 KCCS Career tech Inc. All Rights Reserved.</p>
<!--/footNav--></div>
</footer>

<!--/wrap--></div>

<script src="./js/jquery-1.7.1.min.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/common.js"></script>

</body>
</html>
<?php 
/* ▲▲▲送信完了画面のレイアウト 編集可 ※送信完了後に指定のページに移動しない場合のみ表示▲▲▲　*/
}
//確認画面無しの場合の表示、指定のページに移動する設定の場合、エラーチェックで問題が無ければ指定ページヘリダイレクト
else if(($jumpPage == 1 && $sendmail == 1) || $confirmDsp == 0) { 
	if($empty_flag == 1){ ?>
<div align="center"><h4>入力にエラーがあります。下記をご確認の上「戻る」ボタンにて修正をお願い致します。</h4><div style="color:red"><?php echo $errm; ?></div><br /><br /><input type="button" value=" 前画面に戻る " onClick="history.back()"></div>
<?php 
	}else{ header("Location: ".$thanksPage); }
}

// 以下の変更は知識のある方のみ自己責任でお願いします。

//----------------------------------------------------------------------
//  関数定義(START)
//----------------------------------------------------------------------
function checkMail($str){
	$mailaddress_array = explode('@',$str);
	if(preg_match("/^[\.!#%&\-_0-9a-zA-Z\?\/\+]+\@[!#%&\-_0-9a-z]+(\.[!#%&\-_0-9a-z]+)+$/", "$str") && count($mailaddress_array) ==2){
		return true;
	}else{
		return false;
	}
}
function h($string) {
	global $encode;
	return htmlspecialchars($string, ENT_QUOTES,$encode);
}
function sanitize($arr){
	if(is_array($arr)){
		return array_map('sanitize',$arr);
	}
	return str_replace("\0","",$arr);
}
//Shift-JISの場合に誤変換文字の置換関数
function sjisReplace($arr,$encode){
	foreach($arr as $key => $val){
		$key = str_replace('＼','ー',$key);
		$resArray[$key] = $val;
	}
	return $resArray;
}
//送信メールにPOSTデータをセットする関数
function postToMail($arr){
	global $hankaku,$hankaku_array;
	$resArray = '';
	foreach($arr as $key => $val) {
		$out = '';
		if(is_array($val)){
			foreach($val as $key02 => $item){ 
				//連結項目の処理
				if(is_array($item)){
					$out .= connect2val($item);
				}else{
					$out .= $item . ', ';
				}
			}
			$out = rtrim($out,', ');
			
		}else{ $out = $val; }//チェックボックス（配列）追記ここまで
		if(get_magic_quotes_gpc()) { $out = stripslashes($out); }
		
		//全角→半角変換
		if($hankaku == 1){
			$out = zenkaku2hankaku($key,$out,$hankaku_array);
		}
		if($out != "confirm_submit" && $key != "httpReferer") {
			$resArray .= "【 ".h($key)." 】 ".h($out)."\n";
		}
	}
	return $resArray;
}
//確認画面の入力内容出力用関数
function confirmOutput($arr){
	global $hankaku,$hankaku_array;
	$html = '';
	foreach($arr as $key => $val) {
		$out = '';
		if(is_array($val)){
			foreach($val as $key02 => $item){ 
				//連結項目の処理
				if(is_array($item)){
					$out .= connect2val($item);
				}else{
					$out .= $item . ', ';
				}
			}
			$out = rtrim($out,', ');
			
		}else{ $out = $val; }//チェックボックス（配列）追記ここまで
		if(get_magic_quotes_gpc()) { $out = stripslashes($out); }
		$out = nl2br(h($out));//※追記 改行コードを<br>タグに変換
		$key = h($key);
		
		//全角→半角変換
		if($hankaku == 1){
			$out = zenkaku2hankaku($key,$out,$hankaku_array);
		}
		
		$html .= "<tr><th>".$key."</th><td>".$out;
		$html .= '<input type="hidden" name="'.$key.'" value="'.str_replace(array("<br />","<br>"),"",$out).'" />';
		$html .= "</td></tr>\n";
	}
	return $html;
}

//全角→半角変換
function zenkaku2hankaku($key,$out,$hankaku_array){
	global $encode;
	if(is_array($hankaku_array) && function_exists('mb_convert_kana')){
		foreach($hankaku_array as $hankaku_array_val){
			if($key == $hankaku_array_val){
				$out = mb_convert_kana($out,'a',$encode);
			}
		}
	}
	return $out;
}
//配列連結の処理
function connect2val($arr){
	$out = '';
	foreach($arr as $key => $val){
		if($key === 0 || $val == ''){//配列が未記入（0）、または内容が空のの場合には連結文字を付加しない（型まで調べる必要あり）
			$key = '';
		}elseif(strpos($key,"円") !== false && $val != '' && preg_match("/^[0-9]+$/",$val)){
			$val = number_format($val);//金額の場合には3桁ごとにカンマを追加
		}
		$out .= $val . $key;
	}
	return $out;
}

//管理者宛送信メールヘッダ
function adminHeader($userMail,$post_mail,$BccMail,$to){
	$header = '';
	if($userMail == 1 && !empty($post_mail)) {
		$header="From: $post_mail\n";
		if($BccMail != '') {
		  $header.="Bcc: $BccMail\n";
		}
		$header.="Reply-To: ".$post_mail."\n";
	}else {
		if($BccMail != '') {
		  $header="Bcc: $BccMail\n";
		}
		$header.="Reply-To: ".$to."\n";
	}
		$header.="Content-Type:text/plain;charset=iso-2022-jp\nX-Mailer: PHP/".phpversion();
		return $header;
}
//管理者宛送信メールボディ
function mailToAdmin($arr,$subject,$mailFooterDsp,$mailSignature,$encode,$confirmDsp){
	$adminBody="「".$subject."」からメールが届きました\n\n";
	$adminBody .="＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n\n";
	$adminBody.= postToMail($arr);//POSTデータを関数からセット
	$adminBody.="\n＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n";
	$adminBody.="送信された日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
	$adminBody.="送信者のIPアドレス：".@$_SERVER["REMOTE_ADDR"]."\n";
	$adminBody.="送信者のホスト名：".getHostByAddr(getenv('REMOTE_ADDR'))."\n";
	if($confirmDsp != 1){
		$adminBody.="問い合わせのページURL：".@$_SERVER['HTTP_REFERER']."\n";
	}else{
		$adminBody.="問い合わせのページURL：".@$arr['httpReferer']."\n";
	}
	if($mailFooterDsp == 1) $adminBody.= $mailSignature;
	return mb_convert_encoding($adminBody,"JIS",$encode);
}

//ユーザ宛送信メールヘッダ
function userHeader($refrom_name,$to,$encode){
	$reheader = "From: ";
	if(!empty($refrom_name)){
		$default_internal_encode = mb_internal_encoding();
		if($default_internal_encode != $encode){
			mb_internal_encoding($encode);
		}
		$reheader .= mb_encode_mimeheader($refrom_name)." <".$to.">\nReply-To: ".$to;
	}else{
		$reheader .= "$to\nReply-To: ".$to;
	}
	$reheader .= "\nContent-Type: text/plain;charset=iso-2022-jp\nX-Mailer: PHP/".phpversion();
	return $reheader;
}
//ユーザ宛送信メールボディ
function mailToUser($arr,$dsp_name,$remail_text,$mailFooterDsp,$mailSignature,$encode){
	$userBody = '';
	if(isset($arr[$dsp_name])) $userBody = h($arr[$dsp_name]). " 様\n";
	$userBody.= $remail_text;
	$userBody.="\n＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n\n";
	$userBody.= postToMail($arr);//POSTデータを関数からセット
	$userBody.="\n＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n\n";
	$userBody.="送信日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
	if($mailFooterDsp == 1) $userBody.= $mailSignature;
	return mb_convert_encoding($userBody,"JIS",$encode);
}
//必須チェック関数
function requireCheck($require){
	$res['errm'] = '';
	$res['empty_flag'] = 0;
	foreach($require as $requireVal){
		$existsFalg = '';
		foreach($_POST as $key => $val) {
			if($key == $requireVal) {
				
				//連結指定の項目（配列）のための必須チェック
				if(is_array($val)){
					$connectEmpty = 0;
					foreach($val as $kk => $vv){
						if(is_array($vv)){
							foreach($vv as $kk02 => $vv02){
								if($vv02 == ''){
									$connectEmpty++;
								}
							}
						}
						
					}
					if($connectEmpty > 0){
						$res['errm'] .= "<p class=\"error_messe\">【".h($key)."】は必須項目です。</p>\n";
						$res['empty_flag'] = 1;
					}
				}
				//デフォルト必須チェック
				elseif($val == ''){
					$res['errm'] .= "<p class=\"error_messe\">【".h($key)."】は必須項目です。</p>\n";
					$res['empty_flag'] = 1;
				}
				
				$existsFalg = 1;
				break;
			}
			
		}
		if($existsFalg != 1){
				$res['errm'] .= "<p class=\"error_messe\">【".$requireVal."】が未選択です。</p>\n";
				$res['empty_flag'] = 1;
		}
	}
	
	return $res;
}
//リファラチェック
function refererCheck($Referer_check,$Referer_check_domain){
	if($Referer_check == 1 && !empty($Referer_check_domain)){
		if(strpos($_SERVER['HTTP_REFERER'],$Referer_check_domain) === false){
			return exit('<p align="center">リファラチェックエラー。フォームページのドメインとこのファイルのドメインが一致しません</p>');
		}
	}
}
function copyright(){
	echo '<a style="display:block;text-align:center;margin:15px 0;font-size:11px;color:#aaa;text-decoration:none" href="http://www.php-factory.net/" target="_blank">- PHP工房 -</a>';
}
//----------------------------------------------------------------------
//  関数定義(END)
//----------------------------------------------------------------------
?>